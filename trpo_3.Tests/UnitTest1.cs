using NUnit.Framework;
using System;

namespace TRPO_Lab3.Tests
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void Test()
        {
            trpo_3.Lib.Class1 math_1 = new trpo_3.Lib.Class1();
           
            const double i = 8.3940300000000008d;
            double s = math_1.Formula(2.322, 7.23);
            Assert.AreEqual(i, s);

        }
        [Test]
        public void Test2()
        {

            trpo_3.Lib.Class1 math_1 = new trpo_3.Lib.Class1();
            double s = math_1.Formula(15, 4);
            Assert.Throws<System.ArgumentException>(() => {
                trpo_3.Lib.Class1 math_1 = new trpo_3.Lib.Class1();
                double s = math_1.Formula(-15, -4);
            });
        }
    }
}
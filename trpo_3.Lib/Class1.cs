﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace trpo_3.Lib
{
    public class Class1
    {
        public double Formula(double p, double a)
        {
            if (p <= 0)
            {
                throw new ArgumentException("Периметр не может быть отрицательной");
            }
            if (a <= 0)
            {
                throw new ArgumentException("Апофема не может быть отрицательной");
            }
            return (1d / 2d) * p * a;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using trpo_3.Lib;

namespace TRPO_ASP.Controllers
{
    public class HomeController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Index(string perimetr, string apophema)
        {
            double Square = new Class1().Formula(Convert.ToDouble(perimetr), Convert.ToDouble(apophema));
            ViewBag.result = Square;
            return View();
        }
        
        
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows;
using System.Windows.Input;
using trpo_3.Lib;

namespace TRPO_Lab4.WPF
{
    public class Formula: INotifyPropertyChanged
    {
        public Formula()
		{
            ResultClickCommand = new DelegateCommand(Result_Click);
		}

        public void Result_Click(object param)
        {
            Class1 math_1 = new Class1();
            Result = math_1.Formula(Perimeter, Apothem);
        }

        public ICommand ResultClickCommand { get; set; }
        public double Perimeter { get; set; }

        public double Apothem { get; set; }
        //Переменные для записи результата рассчетов
        public double Result 
        { 
            get { return result; }
            set
			{
                result = value;
				OnPropertyChanged("Result");
			}
        }
        private double result;

		public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}

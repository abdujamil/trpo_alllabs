﻿using System;
using System.Windows;
using TRPO_Lab4.WPF;

namespace TRPO_Lab4.WPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            DataContext = new Formula();
        }

        private void txt_Perimetr_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {

        }

        private void Result_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}

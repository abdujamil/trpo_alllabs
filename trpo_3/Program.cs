﻿using System;

namespace trpo_3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите периметр пирамиды");
            double perimetr = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Введите апофему пирамиды");
            double apofema = Convert.ToDouble(Console.ReadLine());
            if (apofema <= 0)
            {
                Console.WriteLine("Апофема пирамиды не может быть такой");
            }
            else if (perimetr <= 0)
            {
                Console.WriteLine("Периметр пирамиды не может быть отрецательным");
            }
            else
            {

                trpo_3.Lib.Class1 math_1 = new trpo_3.Lib.Class1();
                double answer = math_1.Formula(perimetr, apofema);
                Console.WriteLine($"{answer}");
            }
            Console.ReadKey();
        }
    }
}
